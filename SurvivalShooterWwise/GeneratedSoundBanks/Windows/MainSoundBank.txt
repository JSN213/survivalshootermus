Event	ID	Name			Wwise Object Path	Notes
	743594411	HellephantWalk			\Default Work Unit\HellephantWalk	
	2031599387	HellephantHurt			\Default Work Unit\HellephantHurt	
	2082561468	BunnyHurt			\Default Work Unit\BunnyHurt	
	2538918815	PlayerShoot			\Default Work Unit\PlayerShoot	
	2772844031	BearAttack			\Default Work Unit\BearAttack	
	2810800656	HelliphantAttack			\Default Work Unit\HelliphantAttack	
	3432584883	BunnyAttack			\Default Work Unit\BunnyAttack	
	3475731236	PlayerWalkin			\Default Work Unit\PlayerWalkin	
	3537581393	PlayerHurt			\Default Work Unit\PlayerHurt	
	3656427800	BearHurt			\Default Work Unit\BearHurt	

Modulator Envelope	ID	Name			Wwise Object Path	Notes
	1063636410	Modulator Envelope (Custom)				

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	12037820	hellephantBounce	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\hellephantBounce_59BE5090.wem		\Actor-Mixer Hierarchy\Default Work Unit\enemy_sfx\hellephantBounce		101016
	59832838	player_damage2	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\damage sounds\player_damage2_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\player_sfx\damage_sounds\player_damage2		45424
	152500265	player_damage7	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\damage sounds\player_damage7_12EF4C48.wem		\Actor-Mixer Hierarchy\Default Work Unit\player_sfx\damage_sounds\player_damage7		30020
	215064482	Footsteps02	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps02_3BF75579.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsAttack\Footsteps02		1544
	239262691	Footsteps04	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps04_CD169EDA.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsTails\Footsteps04		13820
	269308311	Bear_growl	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\zombear_growl_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\enemy_sfx\Bear_growl		96540
	364456986	rifle_shot1	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\rifle_shot_914E10B9.wem		\Actor-Mixer Hierarchy\Default Work Unit\player_sfx\rifleShots\shotBegin\rifle_shot1		27034
	373771229	player_damage3	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\damage sounds\player_damage3_3CF7FD61.wem		\Actor-Mixer Hierarchy\Default Work Unit\player_sfx\damage_sounds\player_damage3		91028
	374864871	Helliphant_scream2	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Helliphant_scream2_68F6CC8D.wem		\Actor-Mixer Hierarchy\Default Work Unit\enemy_sfx\Hellephant_screams1\Scream_begin\Helliphant_scream2		35292
	396593500	Footsteps03	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps03_E712D957.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsAttack\Footsteps03		8344
	399886430	player_damage6	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\damage sounds\player_damage6_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\player_sfx\damage_sounds\player_damage6		34380
	406159914	rifle_shot4	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\rifle_shot_14DF1F74.wem		\Actor-Mixer Hierarchy\Default Work Unit\player_sfx\rifleShots\shotTail\rifle_shot4		25920
	515280862	Helliphant_scream4	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Helliphant_scream2_7BB89EC4.wem		\Actor-Mixer Hierarchy\Default Work Unit\enemy_sfx\Hellephant_screams1\Scream_tail\Helliphant_scream4		51612
	537505242	Footsteps04	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps04_F219E84D.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsAttack\Footsteps04		11512
	539516145	rifle_shot2	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\rifle_shot_F61E9EF5.wem		\Actor-Mixer Hierarchy\Default Work Unit\player_sfx\rifleShots\shotBegin\rifle_shot2		17576
	583150255	Bunny_attack	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Bunny_attack_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\enemy_sfx\Bunny_attack		96540
	589056515	rifle_shot3	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\rifle_shot_949223BF.wem		\Actor-Mixer Hierarchy\Default Work Unit\player_sfx\rifleShots\shotTail\rifle_shot3		27032
	629936513	Footsteps05	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps05_08586A83.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsAttack\Footsteps05		1664
	650100422	Footsteps06	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps06_EA897197.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsAttack\Footsteps06		1488
	684318569	Footsteps05	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps05_39E81E42.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsTails\Footsteps05		20860
	693862262	Footsteps01	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps01_BDC227AE.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsTails\Footsteps01		13236
	758088857	Helliphant_scream3	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Helliphant_scream1_A51C6332.wem		\Actor-Mixer Hierarchy\Default Work Unit\enemy_sfx\Hellephant_screams1\Scream_tail\Helliphant_scream3		35132
	760509380	player_damage4	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\damage sounds\player_damage4_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\player_sfx\damage_sounds\player_damage4		36192
	814804038	BearHurt	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\BearHurt_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\enemy_sfx\BearHurt		96540
	817583499	player_damage5	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\damage sounds\player_damage5_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\player_sfx\damage_sounds\player_damage5		70356
	933663960	Footsteps03	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps03_6E0436C1.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsTails\Footsteps03		31536
	962767655	BunnyHurt	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\BunnyHurt_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\enemy_sfx\BunnyHurt		96540
	982850101	HellephantHurt	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\HellephantHurt_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\enemy_sfx\HellephantHurt		192520
	989423267	Footsteps06	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps06_420BF47C.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsTails\Footsteps06		10344
	995575556	Footsteps02	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps02_1C07BCE0.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsTails\Footsteps02		14340
	999363851	Helliphant_scream1	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Helliphant_scream1_694D5076.wem		\Actor-Mixer Hierarchy\Default Work Unit\enemy_sfx\Hellephant_screams1\Scream_begin\Helliphant_scream1		35292
	1054119836	Footsteps01	C:\Users\JSNof\Documents\SurvivalShooter-MUS\SurvivalShooterWwise\.cache\Windows\SFX\Footsteps\Footsteps01_28089F15.wem		\Actor-Mixer Hierarchy\Default Work Unit\footsteps_sfx\WalkinSounds\FootstepsAttack\Footsteps01		1144

