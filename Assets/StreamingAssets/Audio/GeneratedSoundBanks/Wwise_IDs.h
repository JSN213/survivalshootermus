/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BEARATTACK = 2772844031U;
        static const AkUniqueID BEARHURT = 3656427800U;
        static const AkUniqueID BUNNYATTACK = 3432584883U;
        static const AkUniqueID BUNNYHURT = 2082561468U;
        static const AkUniqueID HELLEPHANTHURT = 2031599387U;
        static const AkUniqueID HELLEPHANTWALK = 743594411U;
        static const AkUniqueID HELLIPHANTATTACK = 2810800656U;
        static const AkUniqueID PLAYERHURT = 3537581393U;
        static const AkUniqueID PLAYERSHOOT = 2538918815U;
        static const AkUniqueID PLAYERWALKIN = 3475731236U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MENU_STATE
        {
            static const AkUniqueID GROUP = 3941853002U;

            namespace STATE
            {
                static const AkUniqueID PAUSE = 3092587493U;
                static const AkUniqueID PLAY = 1256202815U;
            } // namespace STATE
        } // namespace MENU_STATE

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAINSOUNDBANK = 534561221U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID ENEMY_SFX = 1195741875U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
        static const AkUniqueID PLAYER_SFX = 817096458U;
        static const AkUniqueID PLAYER_VOICE = 1047305421U;
        static const AkUniqueID SHOOTING_SFX = 3607583154U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
